# symfony-mercure

Push updates to web browsers

## Announcement
* [*HTTP/2: speed up your apps and dispatch real time updates (Symfony and API Platform’s features announcement)*](https://medium.com/@dunglas/http-2-speed-up-your-apps-and-dispatch-real-time-updates-symfony-and-api-platforms-features-76fdc5bf6339)
  2018 Kévin Dunglas